#!/bin/bash
###### collect command line args ######

DEFAULT_COMMAND='docker build .'

COMMAND=''
echo $0

for i in "$@"
do
  COMMAND+="$i "
done

###### setup container ######
service docker start

###### custom command ######

if [ $# -ne 0 ]; then
  $COMMAND
else
  $DEFAULT_COMMAND
fi